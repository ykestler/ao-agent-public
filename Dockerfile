ARG fromTag=lts-alpine-3.17
ARG installerRepo=mcr.microsoft.com/powershell

FROM ${installerRepo}:${fromTag}
COPY ./ingest_service .

RUN pwsh install_modules.ps1
CMD ["pwsh", "./ingest.ps1"]
