function SetSslVerificationFalse {
    add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
        }
    }
"@
    [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
}

function StandardizeLogLevel($LogLevel) {

    if ($null -eq $LogLevel) {
        Write-Host "No log_level env variable detected - seting to INFO"
        $LogLevel = "INFO"
    }
    else {
        $LogLevel = $LogLevel.ToUpper()
    }

    try {
        Add-LoggingTarget -Name Console
        Add-LoggingLevel -Level 5 -LevelName "VERBOSE"
        Add-LoggingLevel -Level 50 -LevelName "NONE"
        Set-LoggingDefaultLevel -Level $LogLevel
    }
    catch {
        Write-Host "Failed to configure logging module"
        Write-Host $_
        exit
    }
}


function EnsureRequiredEnvVars {
    $environment_variables = @{
        "ingest_token" = $env:ingest_token
        "appomni_url" = $env:appomni_url
        "schedule_seconds" = $env:schedule_seconds
        "m365_tenant_id" = $env:m365_tenant_id
        "org_url" = $env:org_url
        "sharepoint_url" = $env:sharepoint_url
    }

    $null_env_vars = @()

    foreach ($key in $environment_variables.Keys) {
        if ($null -eq $environment_variables[$key]) {
            $null_env_vars += $key
        }
    }

    if ($null_env_vars.Length -gt 0) {
        Write-Log -Level "ERROR" -Message ("Missing required environment variable/s: " + ($null_env_vars | out-string))
        exit
    }

    # this is removing the / at the end of the appomni_url if that was added
    if ($env:appomni_url.EndsWith('/')) {
        $env:appomni_url = $env:appomni_url.Substring(0, $env:appomni_url.Length - 1)
    }

    # removes https:// from start of org_url - this has caused issues with connecting to exchange and sharepoint
    if ($env:org_url.StartsWith('https://')) {
        $env:org_url = $env:org_url.Substring(8, $env:org_url.Length - 8)
    }

    # this is ensuring that schedule_seconds is a valid int
    $env:schedule_seconds = $env:schedule_seconds -as [int]
    if ($null -eq $env:schedule_seconds) {
        Write-Log -Level "ERROR" -Message "schedule_seconds environment variable must be valid integer"
        exit
    }
}

function ProcessCertificateFromVault ($Certificate) {
    Write-Log -Level "INFO" -Message "Inserting certificate into cert store"
    $certbytes = [Convert]::FromBase64String($Certificate)
    New-Item -Path 'C:\certs' -ItemType Directory
    Set-Content -Path 'C:\certs\appomni-service-principal-cert.pfx' -Value $certbytes -Encoding Byte
}

function GetCmdletData ($Cmd) {
    try {
        Write-Log -Level "INFO" -Message ("Invoking " + ($Cmd | out-string))
        $data = Invoke-Expression $Cmd
        Write-Log -Level "VERBOSE" -Message ("Data Returned:`n" + ($data | out-string))
        if ($null -eq $data) {
            continue
        }
    }
    catch {
        Write-Log -Level "ERROR" -Message ("Error getting data for cmdlet: " + ($Cmd | out-string))
        Write-Host $_
        continue
    }
    return $data
}
