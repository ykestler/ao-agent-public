function GetCredentialsHashi($HashiVaultPath) {
    $environment_variables = @(
        "ingest_token", "appomni_url", "schedule_seconds" ,"m365_tenant_id", "org_url", "sharepoint_url", "service_principal_id", "use_service_principal", "username", "password", "certificate", "cert_password"
        )
    $ErrorActionPreference = “SilentlyContinue”

    for ($i=0; $i -lt $environment_variables.Length; $i++) {
        $cur_var = $environment_variables[$i]
        Write-Log -Level "INFO" -Message "Pulling $cur_var from hashi if available"
        $val = vault kv get -mount=secret -field="$cur_var" "$HashiVaultPath"
        if ($null -ne $val) {
            [Environment]::SetEnvironmentVariable("$cur_var", "$val", "Process")
        }
    }
    $ErrorActionPreference = “Continue”
}

function GetCredentialsKeyVault($VaultName) {
    $environment_variables = @(
        "ingest_token", "appomni_url", "schedule_seconds" ,"m365_tenant_id", "org_url", "sharepoint_url", "service_principal_id", "use_service_principal", "username", "password", "certificate", "cert_password"
        )
    $ErrorActionPreference = "SilentlyContinue"

    foreach ($cur_var in $environment_variables) {
        Write-Log -Level "INFO" -Message "Pulling $cur_var from key vault if available"

        # Attempt to retrieve version without underscores and add to env variables as the version with underscores to match code logic
        $cur_var_alternate = $cur_var -replace "_", "" 
        $val_alternate = Get-AzKeyVaultSecret -VaultName $VaultName -Name $cur_var_alternate -AsPlainText
        if ($null -ne $val_alternate) {
            Write-Log -Level "INFO" -Message "Pulled $cur_var from key vault"
            [Environment]::SetEnvironmentVariable("$cur_var", "$val_alternate", "Process")
        }
    }
    $ErrorActionPreference = “Continue”
}

function VerifyHashiEnv($VAULT_ADDR, $VAULT_TOKEN, $HashiVaultPath) {
    $hashi_variables = @{
        "VAULT_ADDR" = $VAULT_ADDR
        "VAULT_TOKEN" = $VAULT_TOKEN
        "hashi_vault_path" = $HashiVaultPath
    }
    $hashi_null_env_vars = @()

    foreach ($key in $hashi_variables.Keys) {
        if ($null -eq $hashi_variables[$key]) {
            $hashi_null_env_vars += $key
        }
    }

    if ($hashi_null_env_vars.Length -gt 0) {
        Write-Log -Level "ERROR" -Message ("Missing required environment variable/s: " + ($hashi_null_env_vars | out-string))
        exit
    }
}

function VerifyKvEnv($AzureUsername, $AzurePassword, $VaultName) {
    $kv_variables = @{
        "azure_username" = $AzureUsername
        "azure_password" = $AzurePassword
        "kv_vault_name" = $VaultName
    }
    $kv_null_env_vars = @()

    foreach ($key in $kv_variables.Keys) {
        if ($null -eq $kv_variables[$key]) {
            $kv_null_env_vars += $key
        }
    }

    if ($kv_null_env_vars.Length -gt 0) {
        Write-Log -Level "ERROR" -Message ("Missing required environment variable/s: " + ($kv_null_env_vars | out-string))
        exit
    }
}

function SetUpServicePrincipalAndCert($ServicePrincipalId, $CertFilePath, $CertPassword) {
    if ($null -eq $ServicePrincipalId) {
        Write-Log -Level "ERROR" -Message "If use_service_principal is true, service_principal_id environment variable must be set"
        exit
    }
    if ($null -eq $CertFilePath) {
        $CertFilePath = "C:\certs\appomni-service-principal-cert.pfx"
    }

    if ($null -ne $CertPassword) {
        $pfxpass = $CertPassword | ConvertTo-SecureString -AsPlainText -Force
        Import-PFXCertificate -FilePath $CertFilePath -CertStoreLocation Cert:\CurrentUser\My -Password $pfxpass
        $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($CertFilePath, $pfxpass)
    }
    else {
        $cert = Import-PFXCertificate -FilePath $CertFilePath -CertStoreLocation Cert:\CurrentUser\My
    }
    return @{
        cert = $cert
        pfxpass = $pfxpass
        cert_file_path = $CertFilePath
    }
}

function SetUpBasicAuth ($Username, $Password) {
    if ($null -eq $Username) {
        Write-Log -Level "ERROR" -Message "Missing required 'Username' variable for SetUpBasicAuth"
        exit
    }
    if ($null -eq $Password) {
        Write-Log -Level "ERROR" -Message "Missing required 'Password' variable for SetUpBasicAuth"
        exit
    }
    $secure_password = ConvertTo-SecureString -String $Password -AsPlainText -Force
    $cred = New-Object -TypeName System.Management.Automation.PSCredential -argumentlist $Username,$secure_password

    return $cred

}
