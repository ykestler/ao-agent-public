# needed if using powershell 5 and update EnumsAsStrings below usage to use this filter
# ($body | ConvertTo-Json -EnumsAsStrings -Depth 100)
# we need to convert enumstostrings, but powershell 5 does not have that as a built in capability
Filter ConvertTo-EnumsAsStrings ([int] $Depth = 100, [int] $CurrDepth = 0) {
  if ($_ -is [enum]) {
    $_.ToString()
  }
  elseif ($null -eq $_ -or $_.GetType().IsPrimitive -or $_ -is [string] -or $_ -is [decimal] -or $_ -is [datetime] -or $_ -is [datetimeoffset]) {
    $_
  }
  elseif ($_ -is [Collections.IEnumerable] -and $_ -isnot [Collections.IDictionary]) {
    , ($_ | ConvertTo-EnumsAsStrings -Depth $Depth -CurrDepth ($CurrDepth+1))
  }
  else {
    if ($CurrDepth -gt $Depth) {
      Write-Warning "Recursion depth $Depth exceeded - reverting to .ToString() representations."
      "$_"
    }
    else {
      $oht = [ordered] @{}
      foreach ($prop in $(if ($_ -is [Collections.IDictionary]) {
        $_.GetEnumerator()
        }
        else {
        $_.psobject.properties
        })) {
        if ($prop.Value -is [Collections.IEnumerable] -and $prop.Value -isnot [Collections.IDictionary] -and $prop.Value -isnot [string]) {
          $oht[$prop.Name] = @($prop.Value | ConvertTo-EnumsAsStrings -Depth $Depth -CurrDepth ($CurrDepth+1))
        }
        else {
          $oht[$prop.Name] = $prop.Value | ConvertTo-EnumsAsStrings -Depth $Depth -CurrDepth ($CurrDepth+1)
        }
      }
      $oht
    }
  }
}

function GetPwshCmdlets ($AppomniUrl, $IngestToken) {
    try {
        Write-Log -Level "INFO" -Message "Getting Powershell Cmdlets"
        $cmdlets=Invoke-RestMethod -Method Get -Uri $AppomniUrl/api/v1/o365/o365monitoredservice/pwsh_cmdlets?ingest_token=$IngestToken
        Write-Log -Level "VERBOSE" -Message ("Cmdlets to run:`n" + ($cmdlets | out-string))
    }
    catch {
        Write-Log -Level "ERROR" -Message "Error getting powershell cmdlets"
        Write-Host $_
        if ($_.Exception.Message.Contains("Service Temporarily Unavailable")) {
            Write-Log -Level "INFO" -Message "503 Error - Sleeping for 5 minutes then trying again"
            Start-Sleep 300
            continue
        }
        else {
            exit
        }
    }
    return $cmdlets
}

function PostData ($Data, $Total, $Offset, $Complete, $IngestToken, $Cmd, $IngestServiceVersion, $AppomniUrl) {
    $body = @{
        job_type = "m365"
        description = "push from m365 agent"
        data = $Data
        offset = $Offset
        total = $Total
        complete = $Complete
        ingest_token = $IngestToken
        sync_task_link = $Cmd
        ingest_service_version = $IngestServiceVersion
    }
    try {
        Write-Log -Level "INFO" -Message ("Posting data for " + ($Cmd | out-string) + " to AppOmni")
        Invoke-RestMethod -Method Post -Uri $env:appomni_url/event-collector/custom/v1/ingest/ -Body ($body | ConvertTo-Json -EnumsAsStrings -Depth 100) -ContentType "application/json"
    }
    catch {
        Write-Log -Level "ERROR" -Message ("Error posting data for cmdlet: " + ($Cmd | out-string))
        Write-Host $_
    }
}

function PostPaginatedData ($Data, $Total, $IngestToken, $Cmd, $IngestServiceVersion, $AppomniUrl) {
    $complete = $false
    $offset = 0
    $limit = 10000

    for ($j=0; $j -lt $Total; $j+=10000) {
        if ($offset + $limit -gt $Total) {$complete = $true}
        $sliced_data = $Data[$offset..($offset + $limit - 1)]
        PostData -Data $sliced_data -Total $Total -Offset $offset -Complete $complete -IngestToken $IngestToken -Cmd $Cmd -AppomniUrl $AppomniUrl -IngestServiceVersion $IngestServiceVersion
        $offset+=$limit
    }
}
