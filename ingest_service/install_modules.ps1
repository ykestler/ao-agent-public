if ($true -eq $IsWindows) {
    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
}

# test
# legacy commands from powershell 5
# [Net.ServicePointManager]::SecurityProtocol = "Tls, Tls11, Tls12, Ssl3"
# Install-PackageProvider -Name NuGet -Force -Scope CurrentUser


Install-Module -Name Logging -f -RequiredVersion 4.8.5 -AllowClobber

# updated EXO from 3.4.0 to 3.5.0 
Install-Module -Name ExchangeOnlineManagement  -RequiredVersion 3.5.0 -f -AllowClobber

# Az module has following issue for powershell 5 - would need to pin if using 5
# https://github.com/Azure/azure-powershell/issues/21647
# Install-Module -Name Az.Resources -RequiredVersion 6.15.1 -f -AllowClobber
# Install-Module -Name Az.Accounts -RequiredVersion 2.2.3 -f -AllowClobber
# Install-Module -Name Az.KeyVault -RequiredVersion 4.10.2 -f -AllowClobber

# Since we are now on powershell >7 no need to pin. Testing below
Install-Module -Name Az.Resources -RequiredVersion 7.1.0 -f -AllowClobber
Install-Module -Name Az.Accounts -RequiredVersion 3.0.0 -f -AllowClobber
Install-Module -Name Az.KeyVault -RequiredVersion 6.0.0 -f -AllowClobber


Install-Module -Name PnP.Powershell -RequiredVersion 2.9.0 -f -AllowClobber

# updated teams from 6.0.0 to 6.2.0
Install-Module MicrosoftTeams -RequiredVersion 6.5.0 -f -AllowClobber


# these are for permanently adding tls12 - not needed for anything later than 2016
# Set-ItemProperty -Path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value '1' -Type DWord
# Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value '1' -Type DWord
