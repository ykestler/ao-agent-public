. "$PSScriptRoot\utils.ps1"
. "$PSScriptRoot\get_credentials.ps1"
. "$PSScriptRoot\connect_azure.ps1"
. "$PSScriptRoot\connect_exchange.ps1"
. "$PSScriptRoot\connect_pnp.ps1"
. "$PSScriptRoot\connect_teams.ps1"
. "$PSScriptRoot\ao_helpers.ps1"


$ingest_service_version="0.0.25"
if ($true -eq $IsWindows) {
    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
}

if ($env:ssl_verification -eq "false") {
    SetSslVerificationFalse
}

if ($env:run_as_script -eq "true") {
    . "$PSScriptRoot\install_modules.ps1"
}

StandardizeLogLevel -LogLevel $env:log_level

$exchange_connection = $false
$az_connection = $false
$pnp_connection = $false
$teams_connection = $false

if ($env:use_hashi_vault -eq "true") {
    VerifyHashiEnv -VAULT_ADDR $env:VAULT_ADDR -VAULT_TOKEN $env:VAULT_TOKEN -HashiVaultPath $env:hashi_vault_path
    GetCredentialsHashi -HashiVaultPath $env:hashi_vault_path
}

if ($env:use_key_vault -eq "true") {
    # connect to Az Powershell to get env vars from KV
    if ($env:use_msi -eq "true") {
        $az_connection = ConnectAzMSI
    }
    else {
        VerifyKvEnv -AzureUsername $env:azure_username -AzurePassword $env:azure_password -VaultName $env:kv_vault_name
        $azure_creds = SetUpBasicAuth -Username $env:azure_username -Password $env:azure_password
        $az_connection = ConnectAzBasicOrCert -M365TenantId $env:m365_tenant_id -Cred $azure_creds
    }
    GetCredentialsKeyVault -VaultName $env:kv_vault_name
}

# not passing in env vars as params here - this function is standardizing env vars and has side effects on global vars
EnsureRequiredEnvVars

if ($env:use_msi -eq "true") {
    # Attempting connection through MSI
    $exchange_connection = ConnectExchangeMSI -OrgUrl $env:org_url
    $az_connection = ConnectAzMSI
    $pnp_connection = ConnectPnPMSI -SharePointUrl $env:sharepoint_url -UserAssignedManagedIdentityObjectId $env:uami_principal_id
    $teams_connection = ConnectTeamsMSI -M365TenantId $env:m365_tenant_id
} else {
    if ($null -ne $env:certificate) {
        ProcessCertificateFromVault -Certificate $env:certificate -CertFilePath $env:cert_file_path
    }

    if ($env:use_msi -ne "true" -and $env:use_service_principal -eq "true") {
        $cert_data = SetUpServicePrincipalAndCert -ServicePrincipalId $env:service_principal_id -CertFilePath $env:cert_file_path -CertPassword $env:cert_password
        $cert = $cert_data.cert
        $pfxpass = $cert_data.pfxpass
        $env:cert_file_path = $cert_data.cert_file_path
    } else {
        $cred = SetUpBasicAuth -Username $env:username -Password $env:password
    }
}

if ($false -eq $exchange_connection) {
    # connecting to exchange throws error with Certificate or CertificateThumbprint - must have CertificateFilePath
    ConnectExchangeBasicOrCert -UseServicePrincipal $env:use_service_principal -ServicePrincipalId $env:service_principal_id -CertFilePath $env:cert_file_path -PFXPass $pfxpass -Cred $cred -OrgUrl $env:org_url
}

if ($false -eq $az_connection) {
    ConnectAzBasicOrCert -UseServicePrincipal $env:use_service_principal -Cert $cert -M365TenantId $env:m365_tenant_id -ServicePrincipalId $env:service_principal_id -Cred $cred
}

if ($false -eq $pnp_connection) {
    ConnectPnPBasicOrCert -SharePointUrl $env:sharepoint_url -Cert $cert -OrgUrl $env:org_url -ServicePrincipalId $env:service_principal_id -Cred $cred
}

if ($false -eq $teams_connection) {
    ConnectTeamsBasicOrCert -UseServicePrincipal $env:use_service_principal -Cert $cert -Cred $cred -ServicePrincipalId $env:service_principal_id -M365TenantId $env:m365_tenant_id
}

while ($true) {
    $cmdlets = GetPwshCmdlets -AppomniUrl $env:appomni_url -IngestToken $env:ingest_token

    for ($i=0; $i -lt $cmdlets.Length;  $i++) {
        $cmd = $cmdlets[$i]
        $data = GetCmdletData -Cmd $cmd

        $total = 1
        if ($data.GetType().BaseType.Name -eq "Array") {
            $total = $data.Length
        }
        Write-Log -Level "INFO" -Message ("Count of " + ($cmd | out-string) + ": " + ($total | out-string))

        if ($total -gt 10000) {
            PostPaginatedData -Data $data -Total $total -IngestServiceVersion $ingest_service_version -Cmd $cmd -AppomniUrl $env:appomni_url -IngestToken $env:ingest_token
        }
        else {
            PostData -Data $data -Offset 0 -Total $total -Complete $true -IngestServiceVersion $ingest_service_version -Cmd $cmd -AppomniUrl $env:appomni_url -IngestToken $env:ingest_token
        }

    }
    if ($env:schedule_seconds -eq 0) {
        exit
    }
    Write-Log -Level "INFO" -Message ("Sleeping for " + $env:schedule_seconds + " seconds")
    Start-Sleep $env:schedule_seconds
}
