function ConnectTeamsBasicOrCert ($UseServicePrincipal, $Cert, $ServicePrincipalId, $M365TenantId, $Cred) {
    try {
        Write-Log -Level "INFO" -Message "Connecting to Teams Powershell Module"
        if ($UseServicePrincipal -eq "true") {
            Connect-MicrosoftTeams -CertificateThumbprint $Cert.Thumbprint -ApplicationId $ServicePrincipalId -TenantId $M365TenantId
            return $true
        }
        else {
            Connect-MicrosoftTeams -Credential $Cred -TenantId $M365TenantId
            return $true
        }
    }
    catch {
        Write-Log -Level "ERROR" -Message "Error Connecting to Teams Powershell module."
        Write-Host $_
        Write-Log -Level "INFO" -Message "Teams cmdlets will not be successfully invoked and teams data will not be sent to AppOmni"
        return $false
    }
}

function ConnectTeamsMSI($M365TenantId) {
    try {
        Write-Log -Level "INFO" -Message "Attempting to connect to Teams using MSI"
        Connect-MicrosoftTeams -Identity -TenantId $M365TenantId
        return $true
    }
    catch {
        Write-Host $_
        Write-Log -Level "ERROR" -Message "Failed to connect to Teams using MSI"
        return $false
    }
}
